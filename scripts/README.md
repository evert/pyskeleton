This directory is intended for (nearly) standalone scripts. These are
separate from scripts created by Poetry; the latter are effectively
wrappers around (runnable) modules.
