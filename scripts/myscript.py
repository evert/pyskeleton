#! /usr/bin/env python

"""Script or module to do something"""

from argparse import ArgumentParser
import logging
from pathlib import Path


logger = logging.getLogger(__name__)


def run(inputfile):
    """The main entry point for this script when used as module"""

    # Do the fun stuff here
    print(inputfile)


def configure_logging(loglevel=0):
    """Provide a generic logging configuration from the command line
    options when this script or module is run standalone"""

    levels = {
        "warn": logging.WARNING,
        "info": logging.INFO,
        "debug": logging.DEBUG,
    }
    level = levels[loglevel]
    formatter = logging.Formatter(
        "%(asctime)s - %(module)s:%(funcName)s:%(lineno)d "
        "[%(levelname)s]: %(message)s",
        datefmt="%y-%m-%d %H:%M:%S",
    )
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)

    logger.addHandler(handler)
    logger.setLevel(level)


def parse_args():
    """Parse the command line arguments and options"""

    parser = ArgumentParser()
    parser.add_argument("inputfile", type=Path, help="Input file name")
    parser.add_argument(
        "--loglevel",
        choices=["warn", "info", "debug"],
        default="warn",
        help="Logging level. Note that this is not the verbose level. "
        "The logging level is for programmatic information.",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        default=0,
        action="count",
        help="Verbose output level. Note that this is not the logging "
        "level. The verbose output is for user friendly output.",
    )
    args = parser.parse_args()
    args.verbose = min(args.verbose, 2)

    return args


def main():
    """Main entry point for this script or as a runnable module"""

    args = parse_args()
    configure_logging(args.loglevel)

    run(args.inputfile)


if __name__ == "__main__":
    main()
