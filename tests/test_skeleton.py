import logging

import skeleton.example
from skeleton.logging import configure as configure_logging


logger = logging.getLogger("skeleton")

configure_logging(logging.WARN, logger)


def test_simple():
    skeleton.example.run(None)
