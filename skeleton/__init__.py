"""Base package file. Contains only a few practical variables, such as `__version__`.

Other constants, functions or classes are imported via the `core` module or directly,
using wildcard imports.

"""

__version__ = "VERSION_PLACE_HOLDER"
