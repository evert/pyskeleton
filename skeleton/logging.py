"""Package level logging utilities"""

import logging


def configure(loglevel="warn", logger=None):
    """Provide a generic logging configuration from the command line
    options when this script or module is run standalone"""

    levels = {
        "warn": logging.WARNING,
        "info": logging.INFO,
        "debug": logging.DEBUG,
    }
    if isinstance(loglevel, int):
        level = loglevel
    else:
        level = levels[loglevel]
    formatter = logging.Formatter(
        "%(asctime)s - %(module)s:%(funcName)s:%(lineno)d "
        "[%(levelname)s]: %(message)s",
        datefmt="%y-%m-%d %H:%M:%S",
    )
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)

    # Set up the logger for the package, not just this module
    if not logger:
        logger = logging.getLogger(__package__)
    logger.addHandler(handler)
    logger.setLevel(level)
