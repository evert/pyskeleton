"""The important entrypoint of the package

Most functions and classes are imported here from other modules,
possibly using a wildcard import.

"""

import logging


logger = logging.getLogger(__name__)


def run():
    """Execute the core of the project"""
    logger.info("hello world")
