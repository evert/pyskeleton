"""Script or module to do something"""

from argparse import ArgumentParser
import logging
from pathlib import Path

from . import core


logger = logging.getLogger(__name__)


def run(inputfile):
    """The main entry point for this script when used as module"""

    # Do the fun stuff here
    print(inputfile)

    core.run()


def parse_args():
    """Parse the command line arguments and options"""

    parser = ArgumentParser()
    parser.add_argument("inputfile", type=Path, help="Input file name")
    parser.add_argument(
        "-L",
        "--loglevel",
        choices=["warn", "info", "debug"],
        default="warn",
        help="Logging level. Note that this is not the verbosity level. "
        "The logging level is for programmatic information.",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        default=0,
        action="count",
        help="Verbose output level. Note that this is not the logging "
        "level. The verbose output is for user friendly output. "
        "Multiple uses of this flag increase the verbose level",
    )
    args = parser.parse_args()
    args.verbose = min(args.verbose, 2)

    return args


def main():
    """Main entry point for this script or as a runnable module"""

    args = parse_args()
    configure_logging(args.loglevel)

    run(args.inputfile)


if __name__ == "__main__":
    from .logging import configure as configure_logging

    main()
