"""Module to be able to run the package as a runnable module"""

import argparse
import logging
import pathlib

from .core import run


logger = logging.getLogger(__package__)


def parse_args():
    """Parse the command line arguments and options"""

    parser = argparse.ArgumentParser()
    parser.add_argument("inputfile", type=pathlib.Path, help="Input file name")
    parser.add_argument(
        "-L",
        "--loglevel",
        choices=["warn", "info", "debug"],
        default="warn",
        help="Logging level. Note that this is not the verbosity level. "
        "The logging level is for programmatic information.",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        default=0,
        action="count",
        help="Verbose output level. Note that this is not the logging "
        "level. The verbose output is for user friendly output. "
        "Multiple uses of this flag increase the verbose level",
    )
    args = parser.parse_args()
    args.verbose = min(args.verbose, 2)

    return args


def main():
    """Starting point when used as runnable package"""

    args = parse_args()
    configure_logging(args.loglevel)

    run()


if __name__ == "__main__":
    from .logging import configure as configure_logging

    main()
